package com.emids.health;

public class User {

	
	 private String name;
	    private String gender;
	    private int age;

	    public int getAge() {
	        return age;
	    }

	    public void setAge(int age) {
	        this.age = age;
	    }
	    private HealthParameters health;
	    private HabitParameters habits;

	    public User() {
	    }

	    public User(String name, String gender, int age, HealthParameters health, HabitParameters habits) {
	        setAge(age);
	        setGender(gender);
	        setHabits(habits);
	        setHealth(health);
	        setName(name); // setting these values in constructor because no one can directly access anything getter methods.
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public String getGender() {
	        return gender;
	    }

	    public void setGender(String gender) {
	        this.gender = gender;
	    }

	   

	    public HealthParameters getHealthParameters() {
	        return health;
	    }

	    public void setHealth(HealthParameters health) {
	        this.health = health;
	    }

	    public HabitParameters getHabits() {
	        return habits;
	    }

	    public void setHabits(HabitParameters habits) {
	        this.habits = habits;
	    }
	}