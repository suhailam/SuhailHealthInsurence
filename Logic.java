
package com.emids.health;


public class Logic {
    
   private User client;
   private HabitParameters habit;
   private HealthParameters health;
private User User;
   
    public Logic(User user) {
        setUser(client);
        setHabit(client.getHabits());
        setHealth(client.getHealthParameters());
    }
    
private static int base_Premium=5000;

    Logic() {
    }


    public User getClient() {
        return User;
    }

    public void setUser(User user) {
        this.User = user;
    }

    public HabitParameters getHabit() {
        return habit;
    }

    public void setHabit(HabitParameters habit) {
        this.habit = habit;
    }

    public HealthParameters getHealth() {
        return health;
    }

    public void setHealth(HealthParameters health) {
        this.health = health;
    }

    /**
     * @param args the command line arguments
     */
    public double calculatePercentage(double amount,int percentage){

    return (amount+((percentage*amount)/100));
}

public double genderBased(double amount,String gender)
{
    
if(gender.equalsIgnoreCase(Parameters.MALE)){
return calculatePercentage(amount,Parameters.GENDER_MALE_PERCENTAGE);
}
return amount;
}

   double pre_Existing_Health_Conditions(HealthParameters h,double amount){
       HealthParameters conditions=h;
       int percentage=checkHealthConditions(conditions,amount);
       return calculatePercentage(amount, percentage);
   }
   double pre_Existing_Habit_Conditions(HabitParameters h,double amount){
       HabitParameters conditions=h;
       int percentage=checkHabits(conditions,amount);
       return calculatePercentage(amount, percentage);
   }
    public double calculateBasePremium(double amount,int age)
    {
        int clientAge=age;
        int a=25;
        int b=30;
        int perc=Parameters.BELOW_40_PERCENTAGE;
        double final_Cal=amount;
         if(clientAge<18)
        {
        return amount;
        }
         else if(clientAge>=18 && clientAge<25)
         {
         return calculatePercentage(final_Cal, perc);
         }
       final_Cal=calculatePercentage(final_Cal, perc);
       
       while(a<clientAge){
       if(a>=40){
       perc=Parameters.ABOVE_40_PERCENTAGE;
       }
       
       final_Cal=calculatePercentage(final_Cal, perc);
       a=increment(a,5);
       b=increment(b,5);
       }
       return final_Cal;
    }
     synchronized int increment(int step,int value){
         return value+step;
       }

    public int checkHealthConditions(HealthParameters conditions, double amount) {
        int percentage=0;
      if(conditions.getBlood_Pressure().equalsIgnoreCase(Parameters.YES)){
        percentage+=Parameters.HEALTH_CONDITION_PERCENTAGE;
        }
        if(conditions.getBlood_Sugar().equalsIgnoreCase(Parameters.YES)){
        percentage+=Parameters.HEALTH_CONDITION_PERCENTAGE;
        }
        if(conditions.getHypertension().equalsIgnoreCase(Parameters.YES)){
        percentage+=Parameters.HEALTH_CONDITION_PERCENTAGE;
        }
        if(conditions.getOverweight().equalsIgnoreCase(Parameters.YES)){
        percentage+=Parameters.HEALTH_CONDITION_PERCENTAGE;
        }
    return percentage ;
    }

    public int checkHabits(HabitParameters conditions, double amount) {
         int percentage=0;
        if(conditions.getAlcohol().equalsIgnoreCase(Parameters.YES)){
        percentage+=Parameters.HABIT_BAD_PERCENTAGE;
        }
        if(conditions.getDaily_Exercise().equalsIgnoreCase(Parameters.YES)){
        percentage+=Parameters.HABIT_GOOD_PERCENTAGE;
        }
        if(conditions.getDrugs().equalsIgnoreCase(Parameters.YES)){
        percentage+=Parameters.HABIT_BAD_PERCENTAGE;
        }
        if(conditions.getSmoking().equalsIgnoreCase(Parameters.YES)){
        percentage+=Parameters.HABIT_BAD_PERCENTAGE;
        }
        return percentage;
    }
    
}