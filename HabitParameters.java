package com.emids.health;

public class HabitParameters {
	
	private String smoking;
	private String alcohol;
	private String daily_Exercise;
	private String drugs;
	private User user = new User();

	public HabitParameters(String smoking, String alcohol, String daily_Exercise, String drugs) {
	        this.smoking = smoking;
	        this.alcohol = alcohol;
	        this.daily_Exercise = daily_Exercise;
	        this.drugs = drugs;
	    }

	    public HabitParameters() {
	    }

	    public String getSmoking() {
	        return smoking;
	    }

	    public void setSmoking(String smoking) {
	        this.smoking = smoking;
	    }

	    public String getAlcohol() {
	        return alcohol;
	    }

	    public void setAlcohol(String alcohol) {
	        this.alcohol = alcohol;
	    }

	    public String getDaily_Exercise() {
	        return daily_Exercise;
	    }

	    public void setDaily_Exercise(String daily_Exercise) {
	        this.daily_Exercise = daily_Exercise;
	    }

	    public String getDrugs() {
	        return drugs;
	    }

	    public void setDrugs(String drugs) {
	        this.drugs = drugs;
	    }
	

}
