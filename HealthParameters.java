package com.emids.health;

public class HealthParameters {
	
	 private String hypertension;
     private String blood_Pressure;
    private String blood_Sugar;
    private String overweight;
    
    public  HealthParameters(String hypertension, String blood_Pressure, String blood_Sugar, String overweight) {
        this.hypertension = hypertension;
        this.blood_Pressure = blood_Pressure;
        this.blood_Sugar = blood_Sugar;
        this.overweight = overweight;
    }
   
    public String getHypertension() {
        return hypertension;
    }

    public void setHypertension(String hypertension) {
        this.hypertension = hypertension;
    }

    public String getBlood_Pressure() {
        return blood_Pressure;
    }

    public void setBlood_Pressure(String blood_Pressure) {
        this.blood_Pressure = blood_Pressure;
    }

    public String getBlood_Sugar() {
        return blood_Sugar;
    }

    public void setBlood_Sugar(String blood_Sugar) {
        this.blood_Sugar = blood_Sugar;
    }

    public String getOverweight() {
        return overweight;
    }

    public void setOverweight(String overweight) {
        this.overweight = overweight;
    }
    

	

}
